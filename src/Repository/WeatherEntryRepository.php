<?php

namespace App\Repository;

use App\Entity\WeatherEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method WeatherEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeatherEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeatherEntry[]    findAll()
 * @method WeatherEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherEntryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WeatherEntry::class);
    }

    /**
     * Get all the weather entries
     *
     * @return array|null
     */
    public function getEntries(): ?array
    {
        $qb = $this->createQueryBuilder('w');
        return $qb->getQuery()->getResult();
    }

    /**
     * Save weather entry
     *
     * @param Request $request
     * @return WeatherEntry
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveEntry(Request $request): WeatherEntry
    {
        $entry = new WeatherEntry();

        $entry->setLon($request->get('lon'));
        $entry->setLat($request->get('lat'));
        $entry->setCity($request->get('city'));
        $entry->setTimestamp(new \DateTime());
        $entry->setLookup($request->get('lookup'));
        $entry->setTemp($request->get('temp'));
        $entry->setClouds($request->get('clouds'));
        $entry->setWind($request->get('wind'));
        $entry->setDescription($request->get('description'));

        $em = $this->getEntityManager();
        $em->persist($entry);
        $em->flush();

        return $entry;
    }
}
