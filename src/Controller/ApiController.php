<?php

namespace App\Controller;

use App\Repository\WeatherEntryRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/** @FOSRest\Route("/api") */
class ApiController extends FOSRestController
{
    /**
     * @var WeatherEntryRepository
     */
    private $weatherEntryRepository;

    public function __construct(
        WeatherEntryRepository $weatherEntryRepository
    ) {
        $this->weatherEntryRepository = $weatherEntryRepository;
    }

    /**
     * Get all the weather entries
     *
     * @FOSRest\Get("/entry")
     * @param Request $request
     * @return Response
     */
    public function getEntry(Request $request): Response
    {
        $view = new View();
        $view->setFormat('json');

        try {
            $data = $this->weatherEntryRepository->getEntries();
            $view->setData([
                'error' => 0,
                'data' => $data
            ]);
            $view->setStatusCode(Response::HTTP_OK);
        } catch (\Throwable $e) {
            $view->setData([
                'error' => 1,
                'message' => $e->getMessage()
            ]);
            $view->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Save weather entry
     *
     * @FOSRest\Post("/entry")
     * @param Request $request
     * @return Response
     */
    public function postEntry(Request $request): Response
    {
        $view = new View();
        $view->setFormat('json');

        try {
            $entry = $this->weatherEntryRepository->saveEntry($request);
            $view->setData([
                'error' => 0,
                'data' => $entry
            ]);
            $view->setStatusCode(Response::HTTP_OK);
        } catch (\Throwable $e) {
            $view->setData([
                'error' => 1,
                'message' => $e->getMessage()
            ]);
            $view->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }
}
